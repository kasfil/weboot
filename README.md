<h1 style="text-align: center; color: #FFC107; margin-bottom: 0px; line-height: 0.5; font-family: serif; font-size: 54px">WeBot</h1>
<p style="text-align: center; color: #757575; margin-top: 1px">Web visitor and ads clicker bot</p>

<p style="text-align: center; background-color: #d9d9d9; width: 90vw">A bot to gain passive income from ads provider from your blog or website</p>

#### Features Coverage

-   [x] Process proxies from all file inside folder
-   [ ] Daemonize and detached process
-   [x] Getting multi process for faster time process earning
-   [x] Scrolling trough all page **(Canceled: Done in default)**
-   [x] Clicking defined ads
-   [x] Versatile and easy configure trough dotenv
